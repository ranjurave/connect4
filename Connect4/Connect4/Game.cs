﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connect4
{
    class Game
    {
        public static Game GameInstance;
        private Renderer renderer = new Renderer();
        public static Vector2 BoardSize = new Vector2(7, 6);
        public static char[,] board = new char[BoardSize.x, BoardSize.y];
        public ConsoleKeyInfo Info;
        public bool IsRunning = true;
        public enum PlayerID
        {
            None,
            Yellow,
            Red
        }
        public PlayerID turn = new Random().Next(0, 2) == 0 ? PlayerID.Yellow : PlayerID.Red;
        public PlayerID victor = PlayerID.None;

        public Game()
        {
            GameInstance = this;
            GenerateBoard();
            ProcessGame();
        }

        private void ProcessGame()
        {
            renderer.Render();
            while (IsRunning)
            {
                renderer.MessageLog();
                if (!IsRunning)
                {
                    return;
                }
                //ClearDialogue();
                Info = Console.ReadKey(true);
                Act();
                renderer.Render();
            }
        }

        private void GenerateBoard()
        {
            for (int y = 0; y < BoardSize.y; y++)
            {
                for (int x = 0; x < BoardSize.x; x++)
                {
                    board[x, y] = '-';
                }
            }
        }

        public void Act() {
            if( !int.TryParse(Info.KeyChar.ToString(), out int selectedColumn )||selectedColumn>BoardSize.x ){
                return;
            }
            selectedColumn--;
            if (selectedColumn < 0)
            {
                return;
            }
            
            PlaceTile(selectedColumn);
        }

        private void PlaceTile(int selectedColumn) { 
            for (int i = BoardSize.y-1; i >= 0; i--)
            {
                char c = board[selectedColumn, i];
                if(c == '-') {
                    if (turn == PlayerID.Yellow)
                    {
                        board[selectedColumn, i] = 'X';
                        turn = PlayerID.Red;
                        break;
                    }
                    if (turn == PlayerID.Red)
                    {
                        board[selectedColumn, i] = '0';
                        turn = PlayerID.Yellow;
                        break;
                    }
                    break;
                }
            }
        }

        public void ClearDialogue()
        {
            string Output = "";
            for(int i = 0; i< 17; i++)
            {
                for(int j =0;j< Console.WindowWidth; j++)
                {
                    Output +=" ";
                }
            }
            Console.Write(Output);
        }

    }
}
