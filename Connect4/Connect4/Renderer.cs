﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connect4
{
    class Renderer
    {
        public void Render()
        {
            Console.SetCursorPosition(0, 0);
            RenderHorizontalBoundry();
            //Console.Write("\n");
            RenderBoard();
            Console.Write("\n");
            RenderDivider();
            RenderGuide();
            RenderHorizontalBoundry();
            Console.Write("\n");
        }

        public void RenderHorizontalBoundry()
        {
            //Console.WriteLine("horizontal border");

            for (int i = 0; i <= (Game.BoardSize.x * 2) + 4; i++)
            {
                Console.Write('=');
            }
        }

        public void RenderBoard()
        {
            //Console.WriteLine("board dislayed");
            for (int y = 0; y < Game.BoardSize.y; y++)
            {
                Console.Write("\n|| ");
                for (int x = 0; x < Game.BoardSize.x; x++)
                {
                    Console.Write(Game.board[x, y] + " ");
                }
                Console.Write("||");
            }
        }

        public void RenderGuide()
        {
            Console.Write("|| ");
            for (int i = 0; i < Game.BoardSize.x ; i++)
            {
                Console.Write((i+1) + " ");
            }
            Console.Write("||\n");
        }

        public void RenderDivider()
        {
            Console.Write("||");
            for(int i =0;i<=Game.BoardSize.x *2; i++)
            {
                Console.Write('~');
            }
            Console.Write("||\n");
        }

        public void MessageLog()
        {
            Console.Write("Choose a column from 1 to 7");
            Console.Write("\nNext turn : " + Game.GameInstance.turn.ToString());
        }
    }
}
